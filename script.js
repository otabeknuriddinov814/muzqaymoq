const BuyNow = document.querySelectorAll(".Buy");
const Modal = document.querySelector(".Modal");
const BtnTugma = document.querySelector(".tugma");

const section1 = document.querySelector(".block");
const nav = document.querySelector("#nav");

BuyNow.forEach((el) => {
  el.addEventListener("click", () => {
    Modal.style.display = "block";

    BtnTugma.addEventListener("click", () => {
      Modal.style.display = "none";
    });
  });
});
